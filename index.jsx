"use strict";
// I used the https://pokeapi.co/  API
// I had a lot of fun doing this assignment too!

const {useState, useEffect} = React;

//Makes an opiton for each type
function TypeItem( { type }) {
  return <option className="type-name" id={type.name}>{type.name}</option>;
}


function PokemonDetails({ pokemon }) {

  const [pokemonEntry, setPokemonEntry] = useState(""); //Made a state for the entry of each pokemon

  //If there is no pokemon selected, display a message
  if (!pokemon) {
    return (
      <h1>No Pokémon selected</h1>
    )
  };

  //This is extra -- Finds the entry of each pokemon
  fetch(`https://pokeapi.co/api/v2/pokemon-species/${pokemon.id}`)
  .then( response => {
    if( !response.ok ) { 
      throw new Error("Not 2xx response", {cause: response});
    }         
    return response.json();
  })
  .then(obj => {
    setPokemonEntry(obj.flavor_text_entries[0].flavor_text);
  })
  .catch( err => {})


  return (
    <div>
    <div id="description-container">
      <div id="description">
        <h1>{pokemon.name} #{pokemon.id}</h1>
      <div id="types-container">
        {pokemon.types.map((t) => <p className="type-name" key={t.type.name} id={t.type.name}>{t.type.name}</p>)}
      </div>
        <img src={pokemon.sprites.other["official-artwork"]["front_default"]} />
      </div>
      <div id="stats">
      <h2>Stats</h2>
      <p>Height: {pokemon.height / 10}m</p>
      <p>Weight: {pokemon.weight / 10}kg</p>
      <h3>Base stats:</h3>
      <div>
        {pokemon.stats.map((s) => <p className="stat" key={s.stat.name}>{s.stat.name}: {s.base_stat}</p>)}
      </div>
      </div>
    </div>
    <h3>{pokemonEntry}</h3>
    </div>
  );
}

// Makes an li for each pokemon with their name
function PokemonListItem( { pokemon }) {
  return <li id={pokemon.name} className="pokemon">{pokemon.name}</li>
 }


function App() {
  const [typeList, setTypeList] = useState([]); // For all the types available
  const [currentType, setCurrentType] = useState("normal"); // For the current type selected -- default is normal type
  const [pokemonList, setPokemonList] = useState([]); // For the whole list of pokemon
  const [currentPokemon, setCurrentPokemon] = useState(); // For the polemon curently selected
  const [pokemonDetails, setPokemonDetails] = useState(); // For the details of each Pokemon
  const [searchText, setSearchText] = useState(""); // For the search bar, to search a certain pokemon
  
  //Fetches all the pokemon for the current type selected
  function fetchPokemonList() {
    fetch(`https://pokeapi.co/api/v2/type/${currentType}`)
    .then( response => {
      if( !response.ok ) { 
        throw new Error("Not 2xx response", {cause: response});
      }         
      return response.json();
    })
    .then(obj => {
      setPokemonList(obj.pokemon.map( (index) => index.pokemon));
    })
    .catch( err => {})
  };
  
  //Fetches details of a specific pokemon
  function fetchPokemon() {
    if (!currentPokemon) return;
    fetch(`https://pokeapi.co/api/v2/pokemon/${currentPokemon}`)
    .then( response => {
      if( !response.ok ) { 
        throw new Error("Not 2xx response", {cause: response});
      }         
      return response.json();
    })
    .then(obj => {
      setPokemonDetails(obj);
    })
    .catch( err => {})
  };
  
  /**
   * When the user clicks an li, it removes all the "selected" class from each li then adds the "selected" class to the current li selected 
   */
  function handleClick(e) {
    Array.from(document.getElementsByTagName("li")).forEach((element) => element.classList.remove("selected"));
    if (e.target.className === "pokemon") {
      e.target.classList.add("selected");
       setCurrentPokemon(e.target.id);
     }
   };

  // fetch type list on page load -- From Mr.Sam
  useEffect( () => {
    fetch("https://pokeapi.co/api/v2/type")
    .then( response => { 
      if( !response.ok ) { 
      throw new Error("Not 2xx response", {cause: response});
      }         
      return response.json();
    })
    .then(obj => {
      setTypeList(obj.results.filter((type)=> type.name != "shadow"  && type.name != "unknown")); //There was 2 types in the array that didnt have any pokemon, the types dont even exist so I removed them
    })
      .catch(err => { })
  }, []);
  
  //https://react.dev/reference/react/useEffect
  useEffect(() => fetchPokemonList(), [currentType]); //I had to use currentType as a dependency or it wouldnt change the list
  
  useEffect(() => fetchPokemon(), [currentPokemon]); // Here too i had to use currentPokemon as a dependency or it wouldnt change the detailed view

  return (
    <div className="wrapper">
      <header>
          <h1>Rida's Pokémon Search</h1>
        <form>
          <select id="types-list" onChange={ (e) => setCurrentType(e.target.value)}>
            {typeList.map((type) => (
              <TypeItem type={type}  key={type.name}/>
              ))}
          </select>
          <input type="text" onChange={(e) => {setSearchText(e.target.value.toLowerCase())}}></input>
        </form>     
          <ul onClick={handleClick}>{pokemonList.filter((pokemon)=>pokemon.name.includes(searchText)).map((pokemon) => ( // I tried to make a dynamic search where the user can write a specific pokemon to search
            <PokemonListItem key={pokemon.name} pokemon={pokemon}/>
          ))}
          </ul>
      </header>
      <main>
        <PokemonDetails pokemon={pokemonDetails} />
      </main>
    </div>
  );
}


ReactDOM.render(
  <App />,
  document.querySelector(".react-root")
);
